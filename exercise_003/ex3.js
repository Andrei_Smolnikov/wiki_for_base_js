function createSmartObject(obj) {
  const SMARTNAME = '_smartObject';

  if (Reflect.has(obj, SMARTNAME))
    throw new Error('Property _smartObject already exists.');

  const isObj = val => typeof val === 'object' && val !== null;

  const data = {};
  const mainObj = Symbol('MainObj');
  const getterCatch = new Set();
  const getterNotGarbageCatch = new Set();
  const setterMem = new Set();
  const toDoFunc = new Set();
  const iC = []; // for cyclic references
  const oC = []; // for cyclic references
  let getterCallAdd = false;
  let garbageFlag = false;
  let initGetSet = false;

  // --------------- Wrapping callback function
  const createComputedFunction = (clb, computedNameGS) => {
    createGetterSetter(newObj, computedNameGS); // creating getter hidden by default
    const wrapClb = () => {
      getterCatch.clear();
      getterCallAdd = true; // start record
      const result = clb(newObj);
      getterCallAdd = false; // stop record
      setterMem.forEach(wp => {
        if (wp.wrapClb === wrapClb) setterMem.delete(wp);
      });
      getterCatch.forEach(key => setterMem.add({ key, wrapClb }));
      newObj[computedNameGS] = result;
    };
    return wrapClb;
  };

  // --------------- Creating getter/setter
  const createGetterSetter = (
    dObj,
    key,
    enumOpt = false,
    symKey = Symbol(),
    write = true,
    config = true,
  ) => {
    Reflect.defineProperty(dObj, key, {
      get() {
        if (getterCallAdd) getterCatch.add(symKey);
        if (garbageFlag) getterNotGarbageCatch.add(symKey);
        return data[symKey];
      },
      set(val) {
        if (write || initGetSet) {
          initGetSet = false;
          const startGarbage = Reflect.has(data, symKey); // have data
          const deleteObject = data[symKey];
          data[symKey] = cloneObj(val);

          if (startGarbage) {
            // start garbage collection
            const search = (ot, searchObj, circ = []) => {
              if (!isObj(ot)) return false;
              if (searchObj === ot) return true;
              if (circ.indexOf(ot) >= 0) return false;
              circ.push(ot);
              let result = false;
              Reflect.ownKeys(ot).forEach(otKey => {
                if (otKey !== SMARTNAME) {
                  result = result || search(ot[otKey], searchObj, circ);
                }
              });
              return result;
            };

            getterNotGarbageCatch.clear();
            garbageFlag = true; // start recording
            const deleteFlag = !search(data[mainObj], deleteObject);
            garbageFlag = false; // start recording
            getterNotGarbageCatch.add(mainObj);

            if (isObj(deleteObject) && deleteFlag) {
              const idx = oC.indexOf(deleteObject);
              if (idx >= 0) {
                iC.splice(idx, 1);
                oC.splice(idx, 1);
              }
            }

            Reflect.ownKeys(data).forEach(x => {
              if (!getterNotGarbageCatch.has(x)) {
                const idx = oC.indexOf(data[x]);
                if (idx >= 0) {
                  iC.splice(idx, 1);
                  oC.splice(idx, 1);
                }
                delete data[x];
              }
            });
          } // end garbage collector

          setterMem.forEach(sCatch => {
            if (sCatch.key === symKey) {
              toDoFunc.add(sCatch.wrapClb);
            }
          });
          toDoFunc.forEach(f => {
            toDoFunc.delete(f);
            f();
          });
        } else throw new Error('read only');
      },
      enumerable: enumOpt,
      configurable: config,
    });
  };

  // --------------- Clone Object
  const cloneObj = iObj => {
    if (!isObj(iObj)) return iObj;

    const idx = iC.findIndex(x => x === iObj); // for cyclic references
    if (idx >= 0) return oC[idx];

    const oObj = Object.create(Object.getPrototypeOf(iObj));

    iC.push(iObj);
    oC.push(oObj);

    Reflect.ownKeys(iObj).forEach(key => {
      const desc = Reflect.getOwnPropertyDescriptor(iObj, key);
      if (Reflect.has(desc, 'get')) Reflect.defineProperty(oObj, key, desc);
      else {
        const symKey = Symbol(key);
        createGetterSetter(
          oObj,
          key,
          desc.enumerable,
          symKey,
          desc.writable,
          desc.configurable,
        );
        initGetSet = true;
        oObj[key] = iObj[key];
      }
    });

    return oObj;
  };

  // -------------- Return SmartObject
  const newObj = cloneObj(obj);

  const smartObject = {
    data,
    getterCatch,
    setterMem,
    toDoFunc,
    createComputedFunction,
    createGetterSetter,
    iC,
    oC,
  };
  data[mainObj] = newObj; // link to main object

  Reflect.defineProperty(newObj, '_smartObject', {
    value: smartObject,
    enumearble: false,
  });
  return newObj;
}

function defineComputedField(obj, computedName, clb) {
  if (Reflect.has(obj, computedName))
    throw new Error(`Property "${computedName}" already exists.`);
  const computedNameGS = Symbol(computedName);
  const wrapClb = obj._smartObject.createComputedFunction(clb, computedNameGS);

  Reflect.defineProperty(obj, computedName, {
    get() {
      return this[computedNameGS];
    },
    set() {
      throw new Error('Assignment to computed property');
    },
    enumerable: true,
  });

  wrapClb();
}
